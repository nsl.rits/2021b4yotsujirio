/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2020 Rio Yotsuji
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Rio Yotsuji <is0437pv@ed.ritsumei.ac.jp>
 *         Ritsumeikan University, Shiga, Japan
 */


#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wifi-module.h"
#include "ns3/internet-module.h"
#include "ns3/yotsuji-module.h"
#include "ns3/applications-module.h"
#include "ns3/netanim-module.h"

#include "ns3/ocb-wifi-mac.h"
#include "ns3/wifi-80211p-helper.h"
#include "ns3/wave-mac-helper.h"

#include"ns3/ipv4-address-helper.h"
#include"ns3/ipv4-interface-container.h"
#include"ns3/mobility-helper.h"
#include "ns3/v4ping-helper.h"

#include "ns3/point-to-point-module.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>


NS_LOG_COMPONENT_DEFINE ("YotsujiProtocolMinimum");

using namespace ns3;

Ptr<ConstantPositionMobilityModel> cpmm;

int
main (int argc, char *argv[])
{

   int numNodes = 16; //RSUの数
   int p2pN = 1; //MECサーバ
   int numVehicle = 32; //車両数

   //マップ範囲
   double heightField = 800;
   double widthField = 800;

   ////////////////////// ノード作成 ////////////////////////////////////
   NodeContainer p2pnodes;
   NodeContainer nodes;
   NodeContainer vehicle;
   p2pnodes.Create(p2pN);
   nodes.Create(numNodes);
   vehicle.Create(numVehicle);
   std::cout<<"Nodes created\n";

   ///////////////////// デバイス作成 ////////////////////////////////////

   UintegerValue ctsThr = (true ? UintegerValue (100) : UintegerValue (2200));
   Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", ctsThr);

   // A high number avoid drops in packet due to arp traffic.
   Config::SetDefault ("ns3::ArpCache::PendingQueueSize", UintegerValue (400));

  NqosWaveMacHelper wifi80211pMac = NqosWaveMacHelper::Default();
  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();

  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss("ns3::RangePropagationLossModel","MaxRange", DoubleValue(15));  //通信範囲

  wifiPhy.SetChannel (wifiChannel.Create ());
  Wifi80211pHelper wifi80211p = Wifi80211pHelper::Default();
  wifi80211p.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode", StringValue ("OfdmRate6MbpsBW10MHz"), "RtsCtsThreshold", UintegerValue (0),"ControlMode",StringValue("OfdmRate6MbpsBW10MHz"));

  NetDeviceContainer devices;
  devices = wifi80211p.Install (wifiPhy, wifi80211pMac, nodes);
  devices.Add(wifi80211p.Install (wifiPhy, wifi80211pMac, vehicle));

PointToPointHelper p2p;
   p2p.SetDeviceAttribute("DataRate",StringValue("800Mbps"));
   p2p.SetChannelAttribute("Delay",StringValue("0ms"));

   NetDeviceContainer mleftR; //Router
   NetDeviceContainer mleftL; //Leaf

 //RSUとMECサーバを有線で繋ぐ処理
   for(int i=0;i<=15;i++){
   NetDeviceContainer c = p2p.Install(p2pnodes.Get(0),nodes.Get(i));
   mleftR.Add(c.Get(0));
   mleftL.Add(c.Get(1));
   }

   std::cout<<"Devices installed\n";

   ////////////////////////  モビリティ  ///////////////////////////////////////////

   //範囲設定
   Ptr<UniformRandomVariable> x = CreateObject<UniformRandomVariable>();
   x->SetAttribute ("Min", DoubleValue (0));
   x->SetAttribute ("Max", DoubleValue (widthField));

   Ptr<UniformRandomVariable> y = CreateObject<UniformRandomVariable>();
   y->SetAttribute ("Min", DoubleValue (0));
   y->SetAttribute ("Max", DoubleValue (heightField));

   Ptr<RandomRectanglePositionAllocator> alloc = CreateObject<RandomRectanglePositionAllocator>();
   alloc->SetX (x);
   alloc->SetY (y);
   alloc->AssignStreams(3);



  MobilityHelper mobility;

//RSU16台設置
  uint8_t n=0;
  for(uint8_t i=0;i<=3;i++){
    for(uint8_t j=0;j<=3;j++){
       mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
       mobility.Install(nodes.Get(n));
       cpmm = nodes.Get(n)->GetObject<ConstantPositionMobilityModel> ();
       cpmm->SetPosition(Vector(i*30,j*30,0));
   n++;
   }
  }


/////車両32台生成
int s = 1;
 for(uint8_t j=0;j<=5;j++){
 mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
 mobility.Install(vehicle.Get(j));
 cpmm = vehicle.Get(j)->GetObject<ConstantPositionMobilityModel>();
 cpmm->SetPosition(Vector(0,s*14,0));
 s++;
 }

int t = 1;
 for(uint8_t j=0;j<=1;j++){
 mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
 mobility.Install(vehicle.Get(j+6));
 cpmm = vehicle.Get(j+6)->GetObject<ConstantPositionMobilityModel>();
 cpmm->SetPosition(Vector(t*12,0,0));
 t++;
 }

int k = 1;
 for(uint8_t j=0;j<=5;j++){
 mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
 mobility.Install(vehicle.Get(j+8));
 cpmm = vehicle.Get(j+8)->GetObject<ConstantPositionMobilityModel>();
 cpmm->SetPosition(Vector(30,k*14,0));
 k++;
}



int a = 3;
 for(uint8_t j=0;j<=3;j++){
 mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
 mobility.Install(vehicle.Get(j+14));
 cpmm = vehicle.Get(j+14)->GetObject<ConstantPositionMobilityModel>();
 cpmm->SetPosition(Vector(a*14,30,0));
 a++;
 }

 int b = 3;
  for(uint8_t j=0;j<=3;j++){
  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(vehicle.Get(j+18));
  cpmm = vehicle.Get(j+18)->GetObject<ConstantPositionMobilityModel>();
  cpmm->SetPosition(Vector(b*14,60,0));
  b++;
  }

  int d = 1;
   for(uint8_t j=0;j<=1;j++){
   mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
   mobility.Install(vehicle.Get(j+22));
   cpmm = vehicle.Get(j+22)->GetObject<ConstantPositionMobilityModel>();
   cpmm->SetPosition(Vector(60,d*14,0));
   d++;
  }

  int e = 5;
   for(uint8_t j=0;j<=1;j++){
   mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
   mobility.Install(vehicle.Get(j+24));
   cpmm = vehicle.Get(j+24)->GetObject<ConstantPositionMobilityModel>();
   cpmm->SetPosition(Vector(60,e*14,0));
   e++;
  }

  int f = 1;
   for(uint8_t j=0;j<=3;j++){
   mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
   mobility.Install(vehicle.Get(j+26));
   cpmm = vehicle.Get(j+26)->GetObject<ConstantPositionMobilityModel>();
   cpmm->SetPosition(Vector(90,f*14,0));
   f++;
   }

   int g = 3;
    for(uint8_t j=0;j<=1;j++){
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(vehicle.Get(j+30));
    cpmm = vehicle.Get(j+30)->GetObject<ConstantPositionMobilityModel>();
    cpmm->SetPosition(Vector(g*14,90,0));
    g++;
    }



//MECサーバ１台
 mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
 mobility.Install(p2pnodes.Get(0));
 cpmm = p2pnodes.Get(0)->GetObject<ConstantPositionMobilityModel>();
 cpmm->SetPosition(Vector(150,150,0));


  std::cout<<"mobility set \n";


  //////////////////////// インターネットスタック /////////////////////////

   YotsujiHelper YotsujiProtocol;

   Ipv4ListRoutingHelper listrouting;
   listrouting.Add(YotsujiProtocol, 10);

   InternetStackHelper internet;
   internet.SetRoutingHelper(listrouting);
   internet.Install (nodes);
   internet.Install (p2pnodes);
   internet.Install(vehicle);

   Ipv4AddressHelper ipv4;
   NS_LOG_INFO ("Assign IP Addresses.");
   ipv4.SetBase ("10.1.1.0", "255.255.255.0");

   Ipv4InterfaceContainer mleftLI; //LeafInterface
   Ipv4InterfaceContainer mleftRI; //RouterInterface

      //RSUとMECサーバを有線で繋ぐ処理
      for(int i=0;i<=15;i++){
         NetDeviceContainer ndc;
         ndc.Add(mleftL.Get (i));
         ndc.Add(mleftR.Get (i));
         Ipv4InterfaceContainer ifc = ipv4.Assign (ndc);
         mleftLI.Add(ifc.Get(0));
         mleftRI.Add(ifc.Get(1));
         ipv4.NewNetwork();
      }

      Ipv4InterfaceContainer interfaces;
      interfaces = ipv4.Assign (devices);

   std::cout<<"Internet Stack installed\n";


wifiPhy.EnablePcapAll(std::string("pcap"));

  AnimationInterface anim("gajuanim.xml"); //XMLファイル作成
	Simulator::Stop(Seconds (2000));   
	Simulator::Run ();

    nodes = NodeContainer();
    interfaces = Ipv4InterfaceContainer ();
    devices = NetDeviceContainer ();
    p2pnodes = NodeContainer();
    vehicle = NodeContainer();

    Simulator::Destroy ();
    std::cout<<"end of simulation\n";
}
