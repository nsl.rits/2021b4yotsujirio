卒業研究では,  「MECとリレーネットワークを用いたVANET向けブロックチェーンの提案」について研究を行なった.

<ファイル構成>
NS3フォルダにあるyotsujiフォルダには, doc, examples, helper, modelフォルダがある. examplesにメインコードである「Yotsuji.cc」があり, メインコードでは, ノード数などの変更が可能である. helper, modelフォルダにはそれぞれファイルがあり, パケットサイズや, 通信範囲の設定が可能である.

Bitcoin SImulatorフォルダには, ns3と同様ns-3.25というフォルダがある. Ns-3.25フォルダにはrapidjson, scratch, src, buildフォルダがあり, scratchフォルダにメインコードである「bitcoin-test.cc」がある. メインコードを変更することで, ブロックサイズやノード数などの様々なパラメータを変更することが可能である.


<使用したシミュレータ>
・NS3利用方法(Macbookの場合)： 初めに, Macbook上にNS3のフォルダをインストールする. 最新版をダウンロードすると良い. ターミナルを開き, workspaceフォルダに移動し, ns-3-devのscratchに移動する(/workspace/ns-3-dev/scratch). ターミナルに「./waf configure」を行い, 「./waf ―run Yotsuji」で Yotsuji.ccを実行. 新しいファイルを入れたときに, 「./waf configure」を実行前に行わないとエラーが出る. ターミナルに実行結果が出力される. 他にも, ns-3-devフォルダにpcapファイルが生成され, 送受信時間等を確認できる. 自分が作成したコードはscratch内に入れることで実行可能である.

・Bitcoin Simulator (Macbookの場合)： Bitcoin SimulatorはNS3上に構築することができる. NS3のversionはns-3.25をお勧めする. 他のversionでの実行も可能ではあるが, エラーが多く出ることがある. Macbookにns-3.25をインストールするとうまく起動しない. そこで, Virtual Boxを利用する. Virtual Boxの中にUbuntuをインストールし, Ubuntuにns-3.25をインストールする. インストールしたns-3.25にBitcoin Simulatorをインストールする. ns-allinone-3.25ファイルのns-3.25を開き, scratchフォルダで端末を開く.  NS3と同じで, 「./waf configure」を行い, 「./waf ―run “bitcoin-test”」を実行する. ns-3.25フォルダに実行結果の入ったファイルが作成される. そのファイルには, ブロック生成時間や, どの地域でブロックチェーンが使用されたかなどの細かい結果が出力されている. 自分が作成したコードはscratch内に入れることで実行可能である.

四辻理央のメールアドレス
r.s.love010408@i.softbank.jp
